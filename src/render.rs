//! Render engine
//! 
//! The render engine provides a way to draw components in the terminal.
//! Components should implement the Draw trait.
use termion::clear;
use component::Draw;

/// Screen holds a collection of `Draw`able components.
pub struct Screen {
    /// Contains all drawable components. These get drawn when `draw_all()` is called.
    pub components: Vec<Box<Draw>>,
}

impl Screen {
    /// Draws all components on the terminal.
    pub fn draw_all(&self) {
        print!("{}", clear::All);
        for component in self.components.iter() {
            component.draw();
        }
    }
}   

