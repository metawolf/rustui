//! Standard UI components
//!
//! Provides standard components that can be used to build an interface. 

use termion::{color, cursor};

/// The `Draw` trait provides types that can be drawn on the terminal
pub trait Draw {
    /// Draw the component on the terminal.
    fn draw(&self);
}

/// A button which can be pressed
pub struct Button {
    pub x: u16,
    pub y: u16,
    pub label: String,
}
    
impl Draw for Button {
    fn draw(&self) {
    
        //Because rust uses unicode, we cannot use the ASCII table terminal graphics.
        //In unicode, all the box graphics and stuff can be found at code point U+2500 and onwards
        //The shadow is created with these characters 
        let shadow_upper: char = '\u{2580}';
        let shadow_lower: char = '\u{2584}';
        
        //we're adding two extra spaces so we need to add two extra pieces of shadow
        const EXTRA_BUTTON_SPACE: usize = 2;
        //fg = foreground color, bg = background color
        print!("{position}{fg}{bg} {text} {reset_color}{fg_shadow}{shadow}", 
            position = cursor::Goto(self.x, self.y),  
            fg = color::Fg(color::White),
            bg = color::Bg(color::Blue),
            text = self.label,
            reset_color = color::Bg(color::Reset),
            fg_shadow = color::Fg(color::Black),
            shadow = shadow_lower);
        //print the lower part of the shadow
        print!("{position}{shadow}", 
            position = cursor::Goto(self.x + 1, self.y + 1),
            shadow = shadow_upper.to_string().repeat(self.label.chars().count() + EXTRA_BUTTON_SPACE));
    }
}

pub struct Label {
    pub x: u16,
    pub y: u16,
    pub label: String,
}

impl Draw for Label {
    fn draw(&self) {
         print!("{position}{fg}{bg}{text}", 
            position = cursor::Goto(self.x, self.y),  
            fg = color::Fg(color::Reset),
            bg = color::Bg(color::Reset),
            text = self.label);
    }
}
    
